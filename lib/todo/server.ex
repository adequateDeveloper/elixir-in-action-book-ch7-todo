defmodule Todo.Server do
  use GenServer
  alias Todo.List

  # Example
  # {:ok, todo_server} = Todo.Server.start
  # Todo.Server.add_entry(todo_server, %{date: {2013, 12, 19}, title: "Dentist"})
  # Todo.Server.entries(todo_server, {2013, 12, 19})


  # API

  def start do
    GenServer.start(Todo.Server, nil)
  end

  def add_entry(todo_server, new_entry) do
    GenServer.cast(todo_server, {:add_entry, new_entry})
  end

  def entries(todo_server, date) do
    GenServer.call(todo_server, {:entries, date})
  end


  # Callbacks

  def init(_params) do
    {:ok, List.new}
  end

  def handle_cast({:add_entry, new_entry}, todo_list) do
    new_state = List.add_entry(todo_list, new_entry)
    {:noreply, new_state}
  end

  def handle_call({:entries, date}, _, todo_list) do
    { :reply, List.entries(todo_list, date), todo_list }
  end

end