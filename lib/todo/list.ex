defmodule Todo.List do

  alias Todo.List

  defstruct auto_id: 1, entries: %{}

  def new(), do: %List{}

  def new(entries \\ []) do
    Enum.reduce( entries, %List{}, &add_entry(&2, &1) )
  end

  def add_entry( %List{entries: entries, auto_id: auto_id} = todo_list, entry ) do
    entry = Map.put(entry, :id, auto_id)
    new_entries = Map.put(entries, auto_id, entry)

    %List{todo_list | entries: new_entries, auto_id: auto_id + 1 }
  end

  def entries(%List{entries: entries}, date) do
    entries
      |> Stream.filter( fn({_, entry} ) -> entry.date == date end )
      |> Enum.map( fn({_, entry}) -> entry end )
  end

  def update_entry(todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn(_) -> new_entry end)
  end

  def update_entry( %List{entries: entries} = todo_list, entry_id, updater_fun ) do
    case entries[entry_id] do
      nil -> todo_list
      old_entry ->
        new_entry = updater_fun.(old_entry)
        new_entries = Map.put(entries, new_entry.id, new_entry)
        %List{todo_list | entries: new_entries}
    end
  end

  def delete_entry( %List{entries: entries} = todo_list, entry_id ) do
    %List{todo_list | entries: Map.delete(entries, entry_id)}
  end

end